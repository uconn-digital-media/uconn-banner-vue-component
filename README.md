# uconn-banner-vue-component

This repository contains a single vue component for the uconn banner. The source code was copied from the official `uconn/banner` composer repository and converted into a vue component. Some of the configuration options from the class have been converted to data items in the vue component. These are not Properties because the liklihood of these values changing from page to page within a single site is extremely low. 

The search region has become a slot so that you can put whatever markup you want in it.

## Usage

```html
<template>
...
<uconn-banner>
	<a href="#" class="btn">Apply Now!</a>
	<label for="search">Search</label>
	<input type="text" name="search" id="search">
</uconn-banner>
...
</template>

<script>
import uconnBanner from '~/path/to/uconn-banner.vue'

export default {
	...
	components: {
		...
		uconnBanner
		...
	},
	...
}

</script>
```

